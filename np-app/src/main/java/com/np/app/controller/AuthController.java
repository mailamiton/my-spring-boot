package com.np.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.np.dto.UserDto;
import com.np.service.IAuthService;

@RestController
public class AuthController {

	@Autowired
	private IAuthService authService;

	@RequestMapping(value = "/getUserByUserId", method = RequestMethod.POST)
	public ResponseEntity<Object> getUser(@RequestBody UserDto user) {
		UserDto userDto = null;
		if (user != null && user.getUserName() != null
				&& user.getPassWord() != null) {
			userDto = authService.getUserByUserId(user);
			if (userDto != null) {
				return new ResponseEntity<Object>(userDto, HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(userDto, HttpStatus.NOT_FOUND);
			}
		} else
			return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);

	}

}
