package com.np.dto;

public class UserDto {

	private String userName;
	private String passWord;
	private String firstName;
	private String middleName;
	private String lastName;
	private RoleDto role;
	private String lastLogin;
	private DeviceDto deviceDto;
	private DescribeDto describeDto;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public RoleDto getRole() {
		return role;
	}
	public void setRole(RoleDto role) {
		this.role = role;
	}
	public String getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(String lastLogin) {
		this.lastLogin = lastLogin;
	}
	public DeviceDto getDeviceDto() {
		return deviceDto;
	}
	public void setDeviceDto(DeviceDto deviceDto) {
		this.deviceDto = deviceDto;
	}
	public DescribeDto getDescribeDto() {
		return describeDto;
	}
	public void setDescribeDto(DescribeDto describeDto) {
		this.describeDto = describeDto;
	}
	
	

}
