package com.np.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.np.dao.IUserDao;
import com.np.dto.UserDto;
import com.np.model.Identity.User;

@Service
public class AuthServiceImpl implements IAuthService {

	@Autowired
	private IUserDao userDao;

	public UserDto getUserByUserName(final UserDto user) {
		return null;
	}

	@Override
	public UserDto getUserByUserId(UserDto user) {
		UserDto userdto = null;
		User userfetched = userDao.findOne(Long.parseLong(user.getUserName()));
		if(userfetched != null){
			userdto  = new UserDto();
			userdto.setUserName(userfetched.getUsername());
			userdto.setFirstName(userfetched.getFirstName());
			
		}
		return userdto;
	}

	@Override
	public List<UserDto> getAllUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean createUser(UserDto user) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean createMultipleUser(List<UserDto> userslist) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteUser(UserDto user) {
		// TODO Auto-generated method stub
		return false;
	}

}
