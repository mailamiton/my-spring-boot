package com.np.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.np.service.AuthServiceImpl;

@Configuration
public class ServiceConfiguration {

	@Bean(name = "authService")
	public AuthServiceImpl authService() {
		System.out.println("repo from bean" + "AuthServiceImpl");
		return new AuthServiceImpl();
	}
}
