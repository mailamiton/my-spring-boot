package com.np.service;

import java.util.List;

import com.np.dto.UserDto;


public interface IAuthService {
	
	UserDto getUserByUserName(final UserDto user);
	UserDto getUserByUserId(final UserDto user);
	List<UserDto> getAllUser();
	boolean createUser(final UserDto user);
	boolean createMultipleUser(final List<UserDto> userslist);
	boolean deleteUser(final UserDto user);
}
