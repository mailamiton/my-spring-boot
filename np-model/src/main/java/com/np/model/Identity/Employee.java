package com.np.model.Identity;

public class Employee {

	private String firstName;
	private String lastName;
	private String id;
	private String dept;

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}
	public String getId() {
		return id;
	}
	public void setId(final String id) {
		this.id = id;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(final String dept) {
		this.dept = dept;
	}
}
