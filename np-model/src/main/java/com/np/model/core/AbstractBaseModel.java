package com.np.model.core;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class AbstractBaseModel {

	@Id
	@GeneratedValue
	private Long Id;
	private Date createDt;
    private Date lastUpdDt;
	@Column(name="created_by", nullable=false)
	private String createBy;	
	@Column(name="last_upd_by", nullable=false)
	private String lastUpdBy;
	private boolean isActive;

	public Long getId() {
		return Id;
	}
	public void setId(final Long id) {
		Id = id;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP", nullable=false)
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(final Date createDt) {
		this.createDt = createDt;
	}
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_dt", columnDefinition="TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP", nullable=false)
	public Date getLastUpdDt() {
		return lastUpdDt;
	}
	public void setLastUpdDt(final Date lastUpdDt) {
		this.lastUpdDt = lastUpdDt;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(final String createBy) {
		this.createBy = createBy;
	}
	public String getLastUpdBy() {
		return lastUpdBy;
	}
	public void setLastUpdBy(final String lastUpdBy) {
		this.lastUpdBy = lastUpdBy;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(final boolean isActive) {
		this.isActive = isActive;
	}
	
	
	
}
