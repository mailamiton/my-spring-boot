package com.np.dao;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;

import com.np.model.Identity.User;

public interface IUserDao extends CrudRepository<User, Serializable> {

}
